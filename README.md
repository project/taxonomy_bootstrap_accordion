# Taxonomy Bootstrap Accordion

Provides a Bootstrap accordion for taxonomy vocabularies. When placing your block,
select the vocabularies you want to include in your block. Each vocabulary name
will be the panel heading and its terms will appear below it when expanded.

This module is compatible only with Bootstrap 3 since significant changes were made
between version 2 and 3. It has only been tested with the Bootstrap theme and no
other theme using Bootstrap.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/taxonomy_bootstrap_accordion).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/taxonomy_bootstrap_accordion)

## Requirements

This module requires the following modules:

- [Bootstrap Theme](https://www.drupal.org/project/bootstrap/)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

- Place the accordion menu where you want with block place where you can select the taxonomy
vocabularies that you want to display. The order is controlled by the weight of your vocabularies.

## Maintainers

- Shelane French - [shelane](https://www.drupal.org/u/shelane)
- Justin Shinn - [justinshinn](https://www.drupal.org/u/justinshinn)
- Jim Gauthier - [usurper](https://www.drupal.org/u/usurper)


Lawrence Livermore National Laboratory is operated by Lawrence Livermore
National Security, LLC, for the U.S. Department of Energy, National Nuclear
Security Administration under Contract DE-AC52-07NA27344.

Disclaimer
This document was prepared as an account of work sponsored by an agency of the
United States government. Neither the United States government nor Lawrence
Livermore National Security, LLC, nor any of their employees makes any warranty,
expressed or implied, or assumes any legal liability or responsibility for the
accuracy, completeness, or usefulness of any information, apparatus, product,
or process disclosed, or represents that its use would not infringe privately
owned rights. Reference herein to any specific commercial product, process, or
service by trade name, trademark, manufacturer, or otherwise does not
necessarily constitute or imply its endorsement, recommendation, or favoring by
the United States government or Lawrence Livermore National Security, LLC. The
views and opinions of authors expressed herein do not necessarily state or
reflect those of the United States government or Lawrence Livermore National
Security, LLC, and shall not be used for advertising or product endorsement
purposes.
